﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _2YourDoor.Models;

namespace _2YourDoor.Controllers
{
    public class OrderItemsController : ApiController
    {
        [HttpPut]
        [ResponseType(typeof(List<OrderItemModel>))]
        [Route("api/{orderId:Guid}/OrderItems", Name = "ReplaceOrderItems")]
        public IHttpActionResult UpdateOrderItems(Guid orderId, [FromBody]List<OrderItemModel> orderItems)
        {
            throw new NotImplementedException();
        }

        [HttpPut]
        [ResponseType(typeof(OrderItemModel))]
        [Route("api/{orderId:Guid}/OrderItems/{orderItemId:Guid}", Name = "ReplaceOrderItem")]
        public IHttpActionResult UpdateOrderItem(Guid orderId, Guid orderItemId, [FromBody]OrderItemModel orderItem)
        {
            throw new NotImplementedException();
        }
    }
}
