﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _2YourDoor.Models;
using _2YourDoor.Models.Enums;

namespace _2YourDoor.Controllers
{
    public class OrdersController : ApiController
    {
        [HttpGet]
        [Route("api/orders/{orderId:Guid}", Name = "GetOrder")]
        [ResponseType(typeof(OrderModel))]
        public OrderModel GetOrder(Guid orderId)
        {
            throw new NotImplementedException();
        }


    }
}
