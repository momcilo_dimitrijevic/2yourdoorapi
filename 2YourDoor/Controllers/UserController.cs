﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _2YourDoor.Models;

namespace _2YourDoor.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        [Route("api/user/{userId:Guid}", Name = "GetUserDetails")]
        [ResponseType(typeof(UserDetailsModel))]
        public UserDetailsModel GetUserProfileDetails(Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}
