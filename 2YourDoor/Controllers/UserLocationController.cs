﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _2YourDoor.Models;

namespace _2YourDoor.Controllers
{
    public class UserLocationController : ApiController
    {
        [HttpPut]
        [ResponseType(typeof(HttpResponseMessage))]
        [Route("api/UserLocation/{userId:Guid}", Name = "GetUserLocation")]
        public IHttpActionResult UpdateUserLocation(Guid userId, [FromBody]LocationModel model)
        {
            throw new NotImplementedException();
        }
    }
}
