﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using _2YourDoor.Models;
using _2YourDoor.Models.Enums;

namespace _2YourDoor.Controllers
{
    public class UserOrderController : ApiController
    {
        [HttpPut]
        [ResponseType(typeof(OrderConfirmationStatus))]
        public IHttpActionResult ConfirmUserOrder([FromBody]OrderConfirmationModel orderConfirmation)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [ResponseType(typeof(List<OrderModel>))]
        [Route("api/userOrders/{userId:Guid}", Name = "GetUserOrders")]
        public IHttpActionResult GetUserOrders(Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}
