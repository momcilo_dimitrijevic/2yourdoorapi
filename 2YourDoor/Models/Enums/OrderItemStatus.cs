﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2YourDoor.Models.Enums
{
    public enum OrderItemStatus
    {
        Available,
        Unavailable
    }
}