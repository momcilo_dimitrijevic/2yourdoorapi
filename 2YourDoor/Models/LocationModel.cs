﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2YourDoor.Models
{
    public class LocationModel
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
    }
}