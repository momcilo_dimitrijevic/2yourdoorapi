﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2YourDoor.Models.Enums;

namespace _2YourDoor.Models
{
    public class OrderConfirmationModel
    {
        public Guid UserId { get; set; }
        public Guid OrderId { get; set; }
        public OrderStatus  Status  { get; set; }
    }
}