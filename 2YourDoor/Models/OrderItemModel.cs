﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2YourDoor.Models.Enums;

namespace _2YourDoor.Models
{
    public class OrderItemModel
    {
        public Guid OrderId { get; set; }
        public Guid OrderItemId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }

        public OrderItemModel ReplacementOrderItem { get; set; }

        public OrderItemStatus Status { get; set; }
    }
}