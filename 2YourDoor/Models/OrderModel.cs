﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2YourDoor.Models.Enums;

namespace _2YourDoor.Models
{
    public class OrderModel
    {
        public Guid OrderId { get; set; }
        public List<OrderItemModel> GroceryList { get; set; }
        public LocationModel PickupPoint { get; set; }
        public LocationModel DeliveryPoint { get; set; }
        public OrderStatus Status { get; set; }
    }
}