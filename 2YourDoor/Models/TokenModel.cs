﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2YourDoor.Models
{
    public class TokenModel
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}