﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2YourDoor.Models
{
    public class UserDetailsModel
    {
        public double RevenueThisWeek { get; set; }
        public double RevenueTotal { get; set; }
        public double DeliveriesNumber { get; set; }
        public DateTime NextPaymentDate { get; set; }
    }
}