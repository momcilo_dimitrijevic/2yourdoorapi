﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace _2YourDoor.Utility.Swagger
{
    public class ActionNameFilter : IOperationFilter
    {
        public void Apply(
            Operation operation,
            SchemaRegistry schemaRegistry,
            ApiDescription apiDescription)
        {
            if (operation.parameters != null)
            {
                var names = operation.operationId.Split('_');
                operation.operationId = names[1];
            }
        }
    }
}